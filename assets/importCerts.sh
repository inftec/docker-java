#!/bin/bash

DIR_CERTS=/javacert/certs
JAVA_CACERTS_FILE=$JAVA_HOME/jre/lib/security/cacerts

# Avoid results for empty directory (see https://www.cyberciti.biz/faq/bash-loop-over-file/)
shopt -s nullglob

function doImport() {
  for cert in $DIR_CERTS/*
  do
    echo "Importing certificate $cert"

    keytool -import -alias "$cert" -keystore $JAVA_CACERTS_FILE -file "$cert" -storepass changeit -noprompt
  done
}

if [ -d $DIR_CERTS ]; then
  echo "Importing certificates from $DIR_CERTS"
  doImport
  echo "Import done"
else
  echo "No certificates to import. Link certs to folder $DIR_CERTS to import certificates to Java cacerts"
fi