# Ubuntu Java

Ubuntu 14.08 with oracle java installed

# Available versions

Currently available versions are:

- 1.8.0_121 (tag: 8u171)

## Check for new versions

- docker run --rm -ti inftec/ubuntu-java:8u171 bash
- apt-cache policy oracle-java8-installer

# Certificates

In order to add certificates to truststore, you need to:

- Link a volume containing all certificate files to /javacert/certs (-v /path/to/certs:/javacert/certs)
- Run `bash /javacert/importCerts.sh` during the startup script on the extending container
